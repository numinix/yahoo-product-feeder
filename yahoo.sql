#YAHOO PRODUCT SUBMIT
#

SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration_group
WHERE configuration_group_title= 'Yahoo! Product Submit Feeder Configuration'
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id AND @configuration_group_id != 0;

INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) VALUES (NULL, 'Yahoo! Product Submit Feeder Configuration', 'Set Yahoo! Options', '1', '1');
SET @configuration_group_id=last_insert_id();
UPDATE configuration_group SET sort_order = @configuration_group_id WHERE configuration_group_id = @configuration_group_id;



INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES 
(NULL, 'Yahoo Account Number', 'YAHOO_ACCOUNT_NUMBER', '', 'Enter your Yahoo Account Number', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Yahoo FTP Username', 'YAHOO_USERNAME', 'ftp_username', 'Enter your Yahoo FTP username', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Yahoo FTP Password', 'YAHOO_PASSWORD', 'ftp_password', 'Enter your Yahoo FTP password', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Yahoo Server', 'YAHOO_SERVER', 'ftp.productsubmit.adcentral.yahoo.com', 'Enter Yahoo! server.<br/>Default: ftp.productsubmit.adcentral.yahoo.com', @configuration_group_id, 0, NOW(), NULL, NULL),
(NULL, 'Auction Site Attributes', 'YAHOO_ASA', 'false', 'Activate Auction Site Attributes?', @configuration_group_id, 2, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Display Language', 'YAHOO_LANGUAGE_DISPLAY', 'false', 'Display language in url?', @configuration_group_id, 3, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Language', 'YAHOO_LANGUAGE', 'English', 'What is your sites language?', @configuration_group_id, 3, NOW(), NULL, NULL),
(NULL, 'Display Currency', 'YAHOO_CURRENCY_DISPLAY', 'false', 'Display currency in url?', @configuration_group_id, 4, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Default Currency', 'YAHOO_CURRENCY', 'USD', 'Select currency', @configuration_group_id, 4, NOW(), NULL, 'zen_cfg_pull_down_currencies('),
(NULL, 'Magic SEO URLs', 'YAHOO_MAGIC_SEO_URLS', 'false', 'Output Magic SEO URLs (separate module required)?', @configuration_group_id, 4, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Show Offer ID', 'YAHOO_CODE', 'id', 'A unique alphanumeric identifier for the item - products_id code. ', @configuration_group_id, 6, NOW(), NULL, 'zen_cfg_select_option(array(\'id\', \'model\'),'),
(NULL, 'Show Availability', 'YAHOO_AVAILABILITY_SWITCH', 'false', 'Display products availability?', @configuration_group_id, 7, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Availability', 'YAHOO_AVAILABILITY', '1-2', 'Number of days until shipped?', @configuration_group_id, 7, NOW(), NULL, NULL),
(NULL, 'Include Zero Quantity', 'YAHOO_ZERO_QUANTITY', 'false', 'Include products with zero quantity?', @configuration_group_id, 7, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Condition', 'YAHOO_CONDITION', 'new', 'Choose your default condition', @configuration_group_id, 12, NOW(), NULL, 'zen_cfg_select_option(array(\'new\', \'used\', \'refurbished\'),'),
(NULL, 'Output File Name', 'YAHOO_OUTPUT_FILENAME', 'yahoo.txt', 'Set the name of your Yahoo! output file', @configuration_group_id, 19, NOW(), NULL, NULL),
(NULL, 'Compress Feed File', 'YAHOO_COMPRESS', 'false', 'Compress Google froogle file', @configuration_group_id, 20, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Uploaded date', 'YAHOO_UPLOADED_DATE', '', 'Date and time of the last upload', @configuration_group_id, 21, NOW(), NULL, NULL),
(NULL, 'Output Directory', 'YAHOO_DIRECTORY', 'feed/', 'Set the name of your Yahoo! output directory', @configuration_group_id, 20, NOW(), NULL, NULL),
(NULL, 'Use cPath in url', 'YAHOO_USE_CPATH', 'false', 'Use cPath in product info url', @configuration_group_id, 20, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'),
(NULL, 'Max products', 'YAHOO_MAX_PRODUCTS', '0', 'Default = 0 for infinite # of products', @configuration_group_id, 24, NOW(), NULL, NULL),
(NULL, 'Included Categories', 'YAHOO_POS_CATEGORIES', '', 'Enter category ids separated by commas <br>(i.e. 1,2,3)<br>Leave blank to allow all categories', @configuration_group_id, 30, NOW(), NULL, NULL),
(NULL, 'Excluded Categories', 'YAHOO_NEG_CATEGORIES', '', 'Enter category ids separated by commas <br>(i.e. 1,2,3)<br>Leave blank to deactivate', @configuration_group_id, 30, NOW(), NULL, NULL),
(NULL, 'Manufacturer or Brand', 'YAHOO_BRAND', 'brand', 'Display manufacturer_name as brand or manufacturer?', @configuration_group_id, 31, NOW(), NULL, 'zen_cfg_select_option(array(\'brand\', \'manufacturer\'),'),
(NULL, 'Model or MPN', 'YAHOO_MODEL', 'model', 'Display products_model as model or mpn?', @configuration_group_id, 32, NOW(), NULL, 'zen_cfg_select_option(array(\'model\', \'manufacturer-part-number\'),'),
(NULL, 'Shipping Price', 'YAHOO_SHIPPING', 'calculated', 'Choose fixed, calculated or none', @configuration_group_id, 33, NOW(), NULL, 'zen_cfg_select_option(array(\'fixed\', \'calculated\', \'none\'),'),
(NULL, 'Shipping Surcharge', 'YAHOO_SURCHARGE', '0.00', 'What amount do you want Yahoo! to add to the calculated shipping rate (include decimal)?', @configuration_group_id, 33, NOW(), NULL, NULL);


INSERT INTO admin_pages (`page_key`, `language_key`, `main_page`, `page_params`, `menu_key`, `display_on_menu`, `sort_order`) VALUES
('configYahoo', 'BOX_YAHOO', 'FILENAME_CONFIGURATION', CONCAT('gID', '=', @configuration_group_id), 'configuration', 'Y', 25);

INSERT INTO admin_pages (`page_key`, `language_key`, `main_page`, `page_params`, `menu_key`, `display_on_menu`, `sort_order`) VALUES ('yahooSelect', 'BOX_TOOLS_YAHOO_FEEDER', 'FILENAME_YAHOO', '', 'tools', 'Y', '1');
