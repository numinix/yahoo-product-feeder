<?php
/**
 * yahoo.php
 *
 * @package yahoo product submit
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: yahoo.php 2 2009-07-19 20:06:24Z numinix $
 */

define('FILENAME_YAHOO', 'yahoo');
define('BOX_TOOLS_YAHOO_FEEDER', 'Yahoo Feeder');
define('FILENAME_CONFIGURATION', 'Yahoo data feed');
?>
