<?php
/**
 * yahoo.php
 *
 * @package yahoo product submit
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Portions Copyright 2003-2006 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: yahoo_tools_dhtml.php 2 2009-07-19 20:06:24Z numinix $
 */
$za_contents[] = array('text' => BOX_YAHOO, 'link' => zen_href_link(FILENAME_YAHOO, '', 'NONSSL'));
?>